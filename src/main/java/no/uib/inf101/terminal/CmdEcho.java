package no.uib.inf101.terminal;

public class CmdEcho implements Command {

        @Override
        public String run(String[] array) {
            int lengthOfArray = array.length;
            if (lengthOfArray == 0) {
                return "";
            }
            return String.join(" ", array) + " ";
        }

        @Override
        public String getName() {
            return "echo";
        }
}
